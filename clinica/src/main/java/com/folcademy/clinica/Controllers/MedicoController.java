package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> listarTodo() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.editar(id, dto));
    }

    @PutMapping("/{idMedico}/consulta/{consulta}")
    public ResponseEntity<Boolean> editarConsulta(@PathVariable(name = "idMedico") int id,
                                                  @PathVariable(name = "consulta") int consulta) {
        return ResponseEntity.ok(medicoService.editarConsulta(id, consulta));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }
}
